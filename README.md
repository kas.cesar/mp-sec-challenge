# MP-Sec-Challenge

---

En este repositorio se encuentra todo lo necesario para la deteccion de conecciones anomalas del dataset _Records of Operations on the CoRE Systems_. 

Porfavor lea las instrucciones de instalacion que se muestran acontinuacion para poder efectuar efectivamente este trabajo

## Instalando dependencias

Este proyecto esta pensando para ser ejecutado en un entorno de desarrollo que cuente con python3 en linux. En especifico alguna distribucion basada en ubuntu -en mi caso kubuntu 22.04- en este sentido, estas distribuciones cuentan con python3 instalados por defecto.

Instale pip y git:

`
sudo apt install python3-pip git
`

va a necesitar el paquete "pipenv" que se encuentra en los repositorios de python3

`
pip3 install pipenv
`

clone este repositorio y luego muevase dentro:

`
git clone https://gitlab.com/kas.cesar/mp-sec-challenge.git && cd mp-sec-challenge
`

instancie un entorno de desarrollo con pipenv e instale las dependencias facilemente

`
pipenv shell
`

`
pipenv install
`
... esto puede tomar algun tiempo...



ademas de instalar las dependencias, esto instala jupyter lab, entonces ejecute su entorno jupyter:

`
jupyter lab
`

Ahora navegue hasta el archivo "solution.ipynb" que se encuentra dentro de la carpeta que hemos clonado desde "gitlab" y con doble click abralo. En la barra de herramientas del notebook busca el menu "kernel", abra dicho menu desplegable, en donde debe pinchar la opcion "restart kernel and run all cells".
Tome atencion, y observe como la primera celda le solicitara su password de sudo, esto es para instalar la libreria `graphviz` la cual es una libreria de sistema. si usted no confia en la ejecucion de dicha celda, entonces, pruebe instalandola por fuera en otra terminal con el siguiente codigo:

`
sudo apt install graphviz
`

Luego de esto usted podra visualizar cada resultado obtendio tras la ejecucion de todo el codigo intercalado con explicaciones que ayuda a la comprension de este trabajo y como se abordaron los problemas propuestos por el challenge. __recuerde no ejecutar la primera celda de codigo del notebook si usted hizo este metodo__


Cesar M.

---
